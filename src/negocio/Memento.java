package negocio;

import java.util.ArrayList;

public class Memento
{
	private ArrayList<String> copia;
	
	public Memento(ArrayList<String> arr)
	{
		// Deep copy de la lista
		copia = new ArrayList<String>();
		for(String p: arr)
			copia.add(p);
	}
	
	public ArrayList<String> getArray()
	{
		return copia;
	}
}
